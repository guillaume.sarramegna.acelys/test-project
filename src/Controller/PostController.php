<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @Route("/{page}", requirements={"page"="\d+"})
     */
    public function posts(int $page = 1)
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findBy([], ['id' => 'DESC'], Post::MAX_PER_PAGE, ($page - 1) * Post::MAX_PER_PAGE);

        if (!$posts) {
            throw $this->createNotFoundException('No result found');
        }

        $maxPage = ceil($this->getDoctrine()->getRepository(Post::class)->count([]) / Post::MAX_PER_PAGE);

        return $this->render('post/posts.html.twig', [
            'posts' => $posts,
            'currentPage' => $page,
            'maxPage' => $maxPage,
        ]);
    }

    /**
     * @Route("/post/{slug}")
     */
    public function post(string $slug)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->findOneBySlug($slug);

        if (null === $post || false === $post) {
            throw new NotFoundHttpException('Post not found');
        } else {
            return $this->render('post/post.html.twig', [
                'post' => $post,
            ]);
        }
    }
}
