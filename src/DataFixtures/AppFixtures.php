<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    private const NUMBER_OF_POSTS = 10;

    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < self::NUMBER_OF_POSTS; ++$i) {
            $post = (new Post($faker->sentence))
                ->setContent($faker->realText(1000))
            ;
            $manager->persist($post);
        }

        $manager->flush();
    }
}
